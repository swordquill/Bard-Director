using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private RectTransform timeBar;

    [SerializeField]
    private TMP_Text timeBarTextObject;

    [SerializeField]
    private TMP_Text daysTextObject;

    [SerializeField]
    private float dayTimeMax;

    private PlayerData playerData;

    public float dayTime;

    void Start()
    {
        playerData = GetComponent<PlayerData>();

        dayTime = dayTimeMax;
    }

    void Update()
    {
        
    }

    #region Public

    public void UpdateTime(int hours)
    {
        dayTime -= hours;

        float ratio =  dayTime / dayTimeMax;

        timeBarTextObject.text = (18 + (6 - dayTime)).ToString();

        timeBar.transform.localScale = new Vector3(ratio, timeBar.transform.localScale.y, timeBar.transform.localScale.z);

        if (dayTime <= 0)
        {
            EndDay();
        }
    }


    public void EndDay()
    {
        dayTime = dayTimeMax;

        timeBar.transform.localScale = new Vector3(1, timeBar.transform.localScale.y, timeBar.transform.localScale.z);

        playerData.days += 1;

        daysTextObject.text = playerData.days.ToString();

    }
    #endregion
}
