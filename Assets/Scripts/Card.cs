using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Card
{
    public string cardName;

    public string description;

    public Sprite cardImage;

    public Card()
    {

    }

    public Card(string _cardName, string _description, Sprite _cardImage )
    {
        cardName = _cardName;
        description = _description;
        cardImage = _cardImage;
    }

}
