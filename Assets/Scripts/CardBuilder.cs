using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CardBuilder : MonoBehaviour
{
    [SerializeField]
    private string cardName;

    [SerializeField]
    private TMP_Text titleTextObject;

    [SerializeField]
    private TMP_Text descriptionTextObject;

    [SerializeField]
    private Image imageObject;

    private Card myCard;

    private CardDatabase cardDatabase;

    private void Awake()
    {
        cardDatabase = FindObjectOfType<CardDatabase>();
    }

    private void Start()
    {
        if ( !string.IsNullOrEmpty( cardName ) )
        {
            ConstructCard();
        }
    }

    private void ConstructCard()
    {
        foreach (Card card in cardDatabase.cardDatabase )
        {
            if ( card.cardName.Equals( cardName ) )
            {
                myCard = card;

                break;
            }
        }

        if(myCard != null )
        {
            titleTextObject.text = myCard.cardName;

            descriptionTextObject.text = myCard.description;

            imageObject.sprite = myCard.cardImage;
        } else
        {
            titleTextObject.text = "404";

            descriptionTextObject.text = "Card not found...";
        }

        

    }

}
