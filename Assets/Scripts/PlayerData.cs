using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public int gold;

    public int food;

    public float fame;

    public int days;

}
